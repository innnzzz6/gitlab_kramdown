# frozen_string_literal: true

module GitlabKramdown
  VERSION = '0.4.2'
end
